package com.sky.context;

public class BaseContext {

    public static ThreadLocal<Long> threadLocalEmpId = new ThreadLocal<>();
    public static ThreadLocal<String> threadLocalEmpUserName = new ThreadLocal<>();

//    public static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id) {
        threadLocalEmpId.set(id);
    }

    public static Long getCurrentId() {
        return threadLocalEmpId.get();
    }

    public static void removeCurrentId() {
        threadLocalEmpId.remove();
    }

}
