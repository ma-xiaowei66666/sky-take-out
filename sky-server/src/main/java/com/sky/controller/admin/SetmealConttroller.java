package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.service.impl.SetmealServiceImpl;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/setmeal")
@Api(tags = "套餐管理")
@Slf4j
public class SetmealConttroller {

    @Autowired
    private SetmealService setmealService;

    @PostMapping("/status/{status}")
    @ApiOperation("套餐起售，停售")
    @CacheEvict(cacheNames = "setmealCache",allEntries = true)
    public Result setmeslOpenOrClose(@PathVariable Integer status,Long  id){
        log.info("[套餐起售]status:{},id:{}",status,id);
        setmealService.setmeslOpenOrClose(status,id);
        return Result.success();
    }

    @PutMapping
    @ApiOperation("修改套餐")
    @CacheEvict(cacheNames = "setmealCache",allEntries = true)
    public Result updateSetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("[修改套餐]setmealDTO:{}",setmealDTO);
        setmealService.updateSetmeal(setmealDTO);
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据Id查询套餐")
    public Result<SetmealVO> getById(@PathVariable Long id){
        log.info("[根据Id查询套餐]id:{}",id);
        SetmealVO setmealVO = setmealService.getById(id);
        return Result.success(setmealVO);
    }

    @ApiOperation("删除套餐")
    @DeleteMapping
    @CacheEvict(cacheNames = "setmealCache",allEntries = true)
    public Result delectSetmealById(@RequestParam List<Long> ids){
        log.info("[删除套餐]ids:{}",ids);
        setmealService.delectSetmealById(ids);
           return Result.success();
    }

    @GetMapping("/page")
    @ApiOperation("分页查询")
    public Result<PageResult> gitByPage(SetmealPageQueryDTO setmealPageQueryDTO){
        PageResult pageResult = setmealService.gitByPage(setmealPageQueryDTO);
        return Result.success(pageResult);
    }

    @PostMapping
    @ApiOperation("新增套餐")
    @CacheEvict(cacheNames = "setmealCache",key = "#setmealDTO.categoryId")
    public Result addSetmeal(@RequestBody SetmealDTO setmealDTO) {
        log.info("[新增套餐]setmealDTO:{}",setmealDTO);
        setmealService.addSetmeal(setmealDTO);
        return Result.success();
    }



}





















