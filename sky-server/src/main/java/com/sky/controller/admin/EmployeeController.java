package com.sky.controller.admin;

import com.sky.constant.JwtClaimsConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 员工管理
 * 2b
 */
@RestController
@RequestMapping("/admin/employee")
@Slf4j
@Api(tags = "员工相关接口")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JwtProperties jwtProperties;

    @PutMapping
    @ApiOperation("修改员工信息")
    public Result xiuGaiEmpById(@RequestBody EmployeeDTO employeeDTO){
        log.info("[修改员工信息]员工信息：{}",employeeDTO);
        employeeService.xiuGaiEmpById(employeeDTO);
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("基于ID查询")
    public Result<Employee> byIdEmp(@PathVariable Long id){
        log.info("[基于ID查询]id:{}",id);
        Employee employee = employeeService.byIdEmp(id);
        return Result.success(employee);
    }

    @PostMapping("/status/{status}")
    @ApiOperation("修改员工状态")
    public Result openAndClose(@PathVariable Integer status,Long id){
        log.info("[修改员工状态]status:{},id:{}",status,id);
        employeeService.xiuGaiEmp(status,id);
        return Result.success();
    }


    @GetMapping("/page")
    @ApiOperation("员工分页查询")
    public Result<PageResult> gitByPage(EmployeePageQueryDTO employeeLoginDTO){
        log.info("[员工分页查询]employeeLoginDTO:{}",employeeLoginDTO);
        PageResult pageResult = employeeService.gitByPage(employeeLoginDTO);
        return Result.success(pageResult);
    }



    @PostMapping()
    @ApiOperation("新增员工")
    public Result addEmp(@RequestBody EmployeeDTO employeeDTO){
        log.info("[新增员工] empDTO{}",employeeDTO);
        Long empId = BaseContext.threadLocalEmpId.get();
        String empUserName = BaseContext.threadLocalEmpUserName.get();
        log.info("[当前线程]，thread:{} empId:{} empUserName:{}",Thread.currentThread().getId(),empId,empUserName);
        employeeService.addEmp(employeeDTO);
        return Result.success();
    }

    /**
     * 登录
     *
     * @param employeeLoginDTO
     * @return
     */
    @PostMapping("/login")
    @ApiOperation("登录接口")
    public Result<EmployeeLoginVO> login(@RequestBody EmployeeLoginDTO employeeLoginDTO) {
        log.info("员工登录：{}", employeeLoginDTO);

        Employee employee = employeeService.login(employeeLoginDTO);

        //登录成功后，生成jwt令牌
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID, employee.getId());
        claims.put(JwtClaimsConstant.USERNAME,employee.getUsername());
        String token = JwtUtil.createJWT(
                jwtProperties.getAdminSecretKey(),
                jwtProperties.getAdminTtl(),
                claims);

        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .userName(employee.getUsername())
                .name(employee.getName())
                .token(token)
                .build();

        return Result.success(employeeLoginVO);
    }

    /**
     * 退出
     *
     * @return
     */
    @PostMapping("/logout")
    @ApiOperation("退出接口")
    public Result<String> logout() {
        return Result.success();
    }

}
