package com.sky.controller.admin;

import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(("/admin/shop"))
@Api(tags = "店铺控制器")
public class ShopController {

    public static final String POST_STSTUS = "POST_STSTUS";

    @Autowired
    private RedisTemplate  redisTemplate;

    @PutMapping("/{status}")
    @ApiOperation("营业或打烊")
    public Result setStatus(@PathVariable Integer status){
        log.info("[店铺状态]status:{}",status);
        redisTemplate.opsForValue().set(POST_STSTUS,status);
        return Result.success();
    }

    @GetMapping("/status")
    @ApiOperation("修改营业状态")
    private Result<Integer> getStatus(){
        Integer status = (Integer)redisTemplate.opsForValue().get(POST_STSTUS);
        if (status == null){
            redisTemplate.opsForValue().set(POST_STSTUS,1);
        }
        return Result.success(status);
    }
}
