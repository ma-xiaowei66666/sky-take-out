package com.sky.controller.user;

import com.sky.dto.UserLoginDTO;
import com.sky.result.Result;
import com.sky.service.impl.UserServiceImpl;
import com.sky.vo.UserLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/user")
@Slf4j
@Api(tags = "用户控制器")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @PostMapping("/login")
    public Result<UserLoginVO> loginByOpenId(@RequestBody UserLoginDTO userLoginDTO){
        log.info("[微信登录]userLoginDTO:{}",userLoginDTO);
        UserLoginVO userLoginVO = userService.loginByOpenId(userLoginDTO);
        return Result.success(userLoginVO);
    }
}
