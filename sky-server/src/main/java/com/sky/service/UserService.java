package com.sky.service;

import com.sky.dto.UserLoginDTO;
import com.sky.vo.UserLoginVO;

/**
 * 微信登录
 */
public interface UserService {
    UserLoginVO loginByOpenId(UserLoginDTO userLoginDTO);
}
