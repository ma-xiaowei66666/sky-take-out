package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;

    @Override
    public void addCart(ShoppingCartDTO shoppingCartDTO) {

        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO, shoppingCart);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        log.info("[添加购物车]shoppingCart：{}",shoppingCart);
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByAll(shoppingCart);
        //判断是否为空
        ShoppingCart dbshoppingCart = null;
        if (shoppingCartList != null && shoppingCartList.size() > 0) {
            dbshoppingCart = shoppingCartList.get(0);
            dbshoppingCart.setNumber(dbshoppingCart.getNumber()+1);
            shoppingCartMapper.updateCart(dbshoppingCart);
        }else {
            Long dishId = shoppingCartDTO.getDishId();
            //判断是菜品还是套餐
            if (dishId != null){
                Dish dish = dishMapper.selectById(dishId);
                shoppingCart.setName(dish.getName());
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setAmount(dish.getPrice());
            }else {
                Setmeal setmeal = setmealMapper.selectById(shoppingCartDTO.getSetmealId());
                shoppingCart.setName(setmeal.getName());
                shoppingCart.setImage(setmeal.getImage());
                shoppingCart.setAmount(setmeal.getPrice());
            }
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartMapper.insertById(shoppingCart);
        }

    }

    @Override
    public List<ShoppingCart> findByUserId() {

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUserId(BaseContext.getCurrentId());
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByAll(shoppingCart);
        return shoppingCartList;
    }

    @Override
    public void deleteAllCart() {
        shoppingCartMapper.deleteCart(BaseContext.getCurrentId());
    }
}
