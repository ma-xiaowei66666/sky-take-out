package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.exception.AccountLockedException;
import com.sky.exception.AccountNotFoundException;
import com.sky.exception.PasswordErrorException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //1、根据用户名查询数据库中的数据
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        //
        String MD5Password = DigestUtils.md5DigestAsHex(password.getBytes());
        log.info("MD5Password:{}",MD5Password);
        if (!MD5Password.equals(employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

    @Override
    public void addEmp(EmployeeDTO employeeDTO) {
        Integer status = 1;
        LocalDateTime creatTime = LocalDateTime.now();
        LocalDateTime updateTime = LocalDateTime.now();

        Long empId = BaseContext.getCurrentId();
        String empUserName = BaseContext.threadLocalEmpUserName.get();
        log.info("[当前线程]，thread:{} empId:{} empUserName:{}",Thread.currentThread().getId(),empId,empUserName);

        Long createUser = empId;
        Long updateUser = empId;

        Employee employee = new Employee();
        employee.setName(employeeDTO.getName());
        employee.setUsername(employeeDTO.getUsername());
        employee.setPhone(employeeDTO.getPhone());
        employee.setSex(employeeDTO.getSex());
        employee.setIdNumber(employeeDTO.getIdNumber());
        employee.setStatus(status);
//        employee.setCreateTime(creatTime);
//        employee.setUpdateTime(updateTime);
//        employee.setCreateUser(createUser);
//        employee.setUpdateUser(updateUser);
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));

        employeeMapper.insertEmp(employee);

    }

    @Override
    public PageResult gitByPage(EmployeePageQueryDTO employeeLoginDTO) {
        PageHelper.startPage(employeeLoginDTO.getPage(),employeeLoginDTO.getPageSize());
        Page<Employee> employees = employeeMapper.selectByPageAndName(employeeLoginDTO.getName());
        PageResult pageResult = new PageResult(employees.getTotal(),employees.getResult());
        return pageResult;
    }

    @Override
    public void xiuGaiEmp(Integer status, Long id) {
        Employee employee = new Employee();
        employee.setStatus(status);
        employee.setId(id);
        employeeMapper.updateEmp(employee);
    }

    @Override
    public Employee byIdEmp(Long id) {
        Employee employee = employeeMapper.selectId(id);
        employee.setPassword("******");
        return employee;
    }

    @Override
    public void xiuGaiEmpById(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();

        BeanUtils.copyProperties(employeeDTO,employee);
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(BaseContext.threadLocalEmpId.get());


        employeeMapper.updateEmp(employee);
    }

}
