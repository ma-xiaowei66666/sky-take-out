package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.aop.AotoFill;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select * from employee where username = #{username}")
    Employee getByUsername(String username);

    @Insert("insert into employee\n" +
            "values (#{id},#{username},#{name},#{password},#{phone}," +
            "#{sex},#{idNumber},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    @AotoFill(OperationType.INSERT)
    void insertEmp(Employee employee);

    Page<Employee> selectByPageAndName(String name);

    @AotoFill(OperationType.UPDATE)
    void updateEmp(Employee employee);


    @Select("select * from employee where id = #{id}")
    Employee selectId(Long id);

}
