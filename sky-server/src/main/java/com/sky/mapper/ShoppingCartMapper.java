package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {

    List<ShoppingCart> selectByAll(ShoppingCart shoppingCart);

    void insertById(ShoppingCart dbshoppingCart);


    @Update("update shopping_cart set number = #{number} where id = #{id}")
    void updateCart(ShoppingCart dbshoppingCart);

    @Delete("delete from shopping_cart where user_id = #{currentId}")
    void deleteCart(Long currentId);
}
