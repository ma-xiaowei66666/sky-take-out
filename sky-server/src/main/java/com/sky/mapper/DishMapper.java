package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.aop.AotoFill;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface DishMapper {

    /**
     * 根据分类id查询菜品数量
     * @param categoryId
     * @return
     */
    @Select("select count(id) from dish where category_id = #{categoryId}")
    Integer countByCategoryId(Long categoryId);

    @AotoFill(OperationType.INSERT)
    void insertDish(Dish dish);


    List<DishVO> selectByPage(DishPageQueryDTO dishPageQueryDTO);

    @Select("select * from dish where id = #{id}")
    Dish selectById(Long id);


    Long selectCountByDishIds(List<Long> ids);

    @Delete("delete from dish where id = #{id}")
    void deleteById(Long id);

    @AotoFill(OperationType.UPDATE)
    void updateDishById(Dish dish);

    @Select("select * from dish where category_id = #{categoryId}")
    List<Dish> selectByCategoryId(Long categoryId);

    List<Dish> selectBySetmealId(Long id);

    @Select("select * from dish where category_id  = #{categoryId}")
    List<Dish> list(Dish dish);
}
