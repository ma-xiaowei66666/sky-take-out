package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealDishMapper {


    void insertBatch(@Param("setmealDishes") List<SetmealDish> setmealDishes);

    @Delete("delete from setmeal_dish where setmeal_id = #{id};")
    void deleteSetmealDishById(Long id);



    @Select("select * from setmeal_dish where setmeal_id = #{id};")
    List<SetmealDish> selectById(Long id);
}
